FROM python:3
#select the bse image that is best for our application

#install any operating system junk
WORKDIR /app
#Set the working directory to copy stuff to
COPY accounts accounts
COPY attendees attendees
COPY common common
COPY conference_go conference_go
COPY events events
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py
#Copy all the code from the local directory into the image
RUN pip install -r requirements.txt
#Install any language dependencies
CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi
