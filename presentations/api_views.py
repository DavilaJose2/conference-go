from django.http import JsonResponse
from common.json import ModelEncoder
from events.models import Conference
from .models import Presentation, Status
from django.views.decorators.http import require_http_methods
import json


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = Presentation.objects.fileter()
        return JsonResponse(
            {"presentation": presentation},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            conference=Conference.objects.get(id=conference_id)
            content["conference"]=conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference"},
                status=400
            )

    presentation = Presentation.create(**content)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )










class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request,id):
    presentation = Presentation.objects.get(id=id)
    if request.method == "GET":
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method =="DELETE":
        count, _= Conference.objects.filter(id=id).delete()
        return JsonResponse(
            {"delete": count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if "status" in content:
                status = Status.objects.get(name=content["status"])
                content["status"] =status
        except Status.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid status name"},
                status=400
            )

        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )
