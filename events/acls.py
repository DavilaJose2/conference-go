from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json



def get_photo(city, state):
    query = f"{city}, {state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    headers = {
        "Authorization": PEXELS_API_KEY
    }

    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict["photos"][0]["src"]["original"]
    # if response.status_code == 200:
    #     data = response.json()
    #     photo_url = data["photos"][0]["src"]["original"]
    #     return photo_url
    # else:
    #     return None

def get_weather_data(city, state):
    response = requests.get(
        f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}")
    if response.status_code == 200 and response.json():
       lat, lon = response.json()[0]["lat"], response.json()[0]["lon"]
       weather_response = requests.get(
          f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
        )
       if weather_response.status_code==200:
            weather_data = weather_response.json()
            weather_info = {
                "temp": weather_data["main"]["temp"],
                "description": weather_data["weather"][0]["description"]
            }
            return weather_info
       else:
           return None
    else:
       return None
